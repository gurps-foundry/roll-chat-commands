import SuccessRoll from "gurps-foundry-roll-lib/src/js/Roll/SuccessRoll";
import SuccessRollRenderer from "gurps-foundry-roll-lib/src/js/Renderer/SuccessRollRenderer";

Hooks.on('chatMessage', (log, content, data) => {
    if (content.indexOf('!sr') === 0) {
        let roll = new SuccessRoll({ level: 10, trait: 'Stealth' });
        roll.roll();
        let renderer = new SuccessRollRenderer();

        renderer.render(roll).then((html) => {
            ChatMessage.create({ content: html, user: game.user._id, type: CONST.CHAT_MESSAGE_TYPES.OTHER });
        });
        return false;
    }
});
