const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
    entry: './src/main.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'gurps-foundry-roll-chatcmd.bundle.js',
    },
    plugins: [new CopyWebpackPlugin({ patterns: [{ from: 'static', to: '' }] })],
    devtool: 'source-map',
};
