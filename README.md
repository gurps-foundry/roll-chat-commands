# GURPS Foundry Roll Chat Commands

This small Foundry VTT module provides a handful of chat commands to perform rolls using the [GURPS Foundry Roll Library](https://gitlab.com/gurps-foundry/roll-lib).

The [GURPS Foundry Roll Templates](https://gitlab.com/gurps-foundry/roll-templates) module is required for this module to work!